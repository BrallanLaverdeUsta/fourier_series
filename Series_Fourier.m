function [coeficientes] = Series_Fourier(funcion,periodo,num_coef)

    %Definicion de coeficientes.
    if(mod(num_coef,2)==0)
       inicio=(num_coef/2)*-1;
       final=(inicio*-1)-1;
       numeros=[inicio:final]
    else
        inicio=floor(num_coef/2);
        numeros=[-inicio:inicio] 
    end     

    cof=[];
    
    syms t;
    
    %calculo valores de coeficientes.
    for k = 1:numel(numeros)
        integral=(1/periodo)*int(funcion*exp((-j*2*numeros(k)*pi*t)/periodo),t,[0 periodo]);
        cof=[cof,integral];
    end
    
    %Grafica Modulo
    subplot(1,2,1)
    stem(numeros,abs(cof))
    title('MODULO')
    ylabel('|X(K)|')
    xlabel('K')
    %Grafica Fase
    subplot(1,2,2)
    stem(numeros,angle(cof))
    title('FASE')
    ylabel('ARCTAN(X(K))')
    xlabel('K')
    
    coeficientes=cof;
end

